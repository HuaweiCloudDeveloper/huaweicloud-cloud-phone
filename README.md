# huaweicloud-cloudPhone

#### 介绍
华为云手机开源项目仓主入口

#### 软件架构

![输入图片说明](res/%E7%BB%84%E4%BB%B6%E4%BA%A4%E4%BA%92%E5%BC%80%E6%BA%90%E6%9E%B6%E6%9E%84%E5%9B%BE.PNG)

架构图组件说明：

-  **绿色框** 部分是开源组件，云手机安装 CloudAppEngine 出流程序后，通过 Android-SDK 或者 H5-SDK 收流客户端输入 ip 和 port 能连接云手机，云手机支持通过 CodecPool 远端编码池进行视频流编码
-  **蓝色框** 部分是闭源组件（云手机内置）
-  **橙色框** 部分是需要实现的具体业务模块


#### 组件简介

| 类别   | 名称             | 描述                                                                    | 仓库                                                                          | 状态                                                                          |
|------|----------------|-----------------------------------------------------------------------|-----------------------------------------------------------------------------|-----------------------------------------------------------------------------|
| 端云协同 | Android-SDK    | 作为端云协同的端侧 SDK，用于在 android 设备上访问并接入云端云手机，呈现云手机的画面、声音并操控云手机             | https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloudPhoneAccess-android |  已转维 | 
| 端云协同 | H5-SDK         | 作为端云协同的端侧 SDK，用于在 web 浏览器 上访问并接入云端云手机，呈现云手机的画面、声音并操控云手机               | https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloudPhoneAccess-web     |  已转维 | 
| 编码池化 | CodecPool | 云手机外置编码资源池 | https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloud-phone-codec-pool |   已转维 | 
| AOSP | AOSP14 | 云手机 AOSP14 开源代码，用于构建运行在华为云手机服务器的 Android14 镜像 | https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloud-phone-aosp14 | 研发中 | 
| AOSP | AOSP11 | 云手机 AOSP11 开源代码，用于构建运行在华为云手机服务器的 Android11 镜像 | https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloudPhone-aosp11 | 已转维 | 
| AOSP | AOSP9 | 云手机 AOSP9 开源代码，用于构建运行在华为云手机服务器的 Android9 镜像 | https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloud-phone-aosp9 |  已转维 | 
| AOSP | AOSP7 | 云手机 AOSP7 开源代码，用于构建运行在华为云手机服务器的 Android7 镜像 | https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloud-phone-aosp7 | 已转维 | 


#### 使用说明 - 端云协同

##### 端口配置

购买服务器时配置应用端口，建议客户按照如下配置，其中 7000 和 7001 为 CloudAppEngine 默认监听端口，分别用于 Android-SDK 和 H5-SDK 接入，其他三个端口可结合业务自行配置。

![输入图片说明](res/%E7%AB%AF%E5%8F%A3%E9%85%8D%E7%BD%AE%E7%A4%BA%E4%BE%8B.png)

##### 快速体验

想要快速体验试用，看到云手机画面，在购买好服务器（配置了 7000 和 7001、 **7002** 端口）后可按如下操作进行体验。

1、启动 CloudAppEngine 进行推流（云手机端出流程序）

 - a)  确认云手机镜像是2023-06-07后发布的镜像 ([官网发布镜像](https://support.huaweicloud.com/wtsnew-cph/cph_wtsnew_0004.html))，如果不是请在 华为云手机控制台-实例管理-对应云手机实例的操作-重启或重置更换镜像。

 - b）在华为云手机控制台-实例管理-对应云手机实例的操作-更新云手机属性里，勾选 ro.com.cph.cloud_app_engine 属性，并选择是。

 - c）在华为云手机控制台-实例管理-云手机详情页面查询接入 ip 和 port
![输入图片说明](res/%E6%9F%A5%E8%AF%A2%E6%8E%A5%E5%85%A5ip%E5%92%8C%E7%AB%AF%E5%8F%A3%E7%A4%BA%E4%BE%8B.png)

2、安装 Android 客户端（Android 客户端收流应用）

 - a）从以下发行版中下载最新的客户端 APK 文件：
   

        https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloudPhoneAccess-android/releases  
   
  
 - b）安装：按照 Android 应用 APK 安装方式安装下载好的APK应用    

  
 - c）使用：安装成功之后，输入上述查询的 ip 和 port 进行连接   

3、Web 客户端（Web 客户端收流程序，与 Android 客户端功能相同）

 - a）从以下发行版本中下载最新的版本 ：         


        https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloudPhoneAccess-web/releases

 - b）使用：解压 rar 文件，修改 demo.html 里的 ip 和 port，具体操作请参考 demo.md 文档   

##### 快速开发

生产环境发布需要整个业务流程闭环，尤其是 CloudAppEngine 向鉴权服务进行接入鉴权校验、向调度服务上报心跳、客户端接入和退出事件这几个环节，需要在 CloudAppEngine  [配置鉴权服务和调度服务的地址](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloudPhoneAccess-engine#51-%E9%85%8D%E7%BD%AE%E7%BB%84%E4%BB%B6%E4%BA%A4%E4%BA%92%E5%9B%BE%E4%B8%AD-remoteserver-%E9%89%B4%E6%9D%83%E8%AE%A4%E8%AF%81%E6%9C%8D%E5%8A%A1%E8%B0%83%E5%BA%A6%E6%9C%8D%E5%8A%A1--%E5%AF%B9%E5%BA%94%E7%9A%84-url-%E5%9C%B0%E5%9D%80)

##### 进阶使用

端云协同支持的音视频出流、触控指令注入的基础能力，可以满足绝大多数的业务场景。

如有相关特殊业务场景需要，可以选择性参考使用以下支持的增强能力：

- 可用于登录和支付的数据通道
- 摄像头仿真
- 麦克风仿真
- 远程输入法（Android）


#### 开源演进规划

- 远程输入法 (Web)
- 资源调度管理平台


#### 开源合作

欢迎有能力且感兴趣的开发者们加入。


